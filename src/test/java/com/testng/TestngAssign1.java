package com.testng;

import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class TestngAssign1 {
	WebDriver driver=new ChromeDriver();
	
	@BeforeClass
	public void lauchBrowser() {
    driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(5));
	driver.get("https://demowebshop.tricentis.com/");
	driver.manage().window().maximize();
	}

  @Test(priority=1)
  public void login() {
	  driver.findElement(By.linkText("Log in")).click();
	  driver.findElement(By.id("Email")).sendKeys("swathivenkat@gmail.com");
	  driver.findElement(By.id("Password")).sendKeys("swathi123");
	  driver.findElement(By.xpath("//input[@value='Log in']")).click();
	  WebElement text = driver.findElement(By.linkText("swathivenkat@gmail.com"));
	  System.out.println(text);
  }
  
  @Test(priority=2)
  public void searchProduct() {
	  WebElement computer=driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Computers')]"));
	  Actions ac=new Actions(driver);
	  ac.moveToElement(computer).build().perform();
	  ac.moveToElement(driver.findElement(By.xpath("//ul[@class='top-menu']//a[contains(text(),'Desktops')]"))).click().perform();
  }
  
  @Test(priority=3)
  public void sortproduct() {
	  WebElement e1=driver.findElement(By.id("products-orderby"));
	  Select sc=new Select(e1);
	  sc.selectByVisibleText("Price: Low to High");
  }
  
  
  @Test(priority=4)
  public void addToCart() {
	  driver.findElement(By.xpath("(//input[@value='Add to cart'])[1]")).click();
	  driver.findElement(By.id("add-to-cart-button-72")).click();
	  driver.findElement(By.xpath("//span[contains(text(),'Shopping cart')]")).click();
	  driver.findElement(By.id("termsofservice")).click();
	  driver.findElement(By.id("checkout")).click();
  }
  
  @Test(priority=5)
  public void shippingDetails() throws InterruptedException {
	  Thread.sleep(7000);
	  driver.findElement(By.xpath("(//input[@value='Continue'])[1]")).click();
	  Thread.sleep(4000);
	  driver.findElement(By.xpath("(//input[@value='Continue'])[2]")).click();	
	  Thread.sleep(4000);
	  driver.findElement(By.xpath("(//input[@value='Continue'])[3]")).click();
	  Thread.sleep(4000);
	  driver.findElement(By.xpath("(//input[@value='Continue'])[4]")).click();
	  Thread.sleep(4000);
	  driver.findElement(By.xpath("(//input[@value='Continue'])[5]")).click();
	  Thread.sleep(4000);
	  driver.findElement(By.xpath("//input[@value='Confirm']")).click();
	  Thread.sleep(4000);
  }
  
  @Test(priority=6)
  public void orderStatus() {
	  WebElement txt = driver.findElement(By.xpath("//*[contains(text(),'Your order has been successfully processed!')]"));
	  System.out.println(txt.getText());
  }
  
  @AfterClass
  public void logOutAndClose() {
	  driver.findElement(By.xpath("//a[contains(text(),'Log out')]")).click();
	  driver.close();
  }
  
  
  
  
  
}











